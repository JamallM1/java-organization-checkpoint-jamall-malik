package com.galvanize;

public class Booking {
    private RoomType roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;
    enum RoomType {
        Conference_Room,Suite,Auditorium, Classroom
        }


    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime){
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;

    }
    static Booking parse(String bookingCode) {
        String[] splitBookingCode = bookingCode.split("-");
        String roomAbbreviation = String.valueOf(splitBookingCode[0].charAt(0));
        RoomType roomType = null;
        String roomNumber = splitBookingCode[0].replace(roomAbbreviation, "");
        String startTime = splitBookingCode[1];
        String endTime = splitBookingCode[2];

        if(roomAbbreviation.equals("r")){
            roomType= RoomType.Conference_Room;
        }
        if(roomAbbreviation.equals("s")){
            roomType= RoomType.Suite;
        }
        if(roomAbbreviation.equals("a")){
            roomType= RoomType.Auditorium;
        }
        if(roomAbbreviation.equals("c")){
            roomType= RoomType.Classroom;
        }
        return new Booking(roomType, roomNumber, startTime, endTime);
    }


    public RoomType getRoomType() {
        return roomType;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    public String getStartTime() {
        return startTime;
    }
    public String getEndTime() {
        return endTime;
    }

}
