package com.galvanize;

import formatters.CSVFormatter;
import formatters.Formatter;
import formatters.HTMLFormatter;
import formatters.JSONFormatter;


public class Application {
    public static void main(String[] args) {
        //System.out.println("Application Running");
        Formatter formatter = getFormatter(args[1]);
        Booking booking = Booking.parse(args[0]);

        System.out.print(formatter.format(booking));

    }

    public static Formatter getFormatter(String format) {
        switch (format) {
            case "json":
                return new JSONFormatter();
            case "csv":
                return new CSVFormatter();
            case "html":
                return new HTMLFormatter();
            default:
                return null;
        }

    }
}