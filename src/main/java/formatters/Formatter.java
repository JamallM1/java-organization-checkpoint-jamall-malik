package formatters;
import com.galvanize.Booking;

public interface Formatter {
    String format(Booking booking);
}
