package formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter {
    @Override
    public String format(Booking booking) {

        return "<dl>\n" +
                String.format("  <dt>Type</dt><dd>%s</dd>\n", String.valueOf(booking.getRoomType()).replace("_", " ")) +
                String.format("  <dt>Room Number</dt><dd>%s</dd>\n", booking.getRoomNumber()) +
                String.format("  <dt>Start Time</dt><dd>%s</dd>\n", booking.getStartTime()) +
                String.format("  <dt>End Time</dt><dd>%s</dd>\n", booking.getEndTime()) +
                "</dl>";
    }
}

