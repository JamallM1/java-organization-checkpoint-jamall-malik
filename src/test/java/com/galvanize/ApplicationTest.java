package com.galvanize;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {
    PrintStream original;
    ByteArrayOutputStream outContent;

    private Application app;
    private Booking booking;



    // This block captures everything written to System.out
    @BeforeEach
    public void setUp() {
        app = new Application();
        //booking = new Booking();
    }
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void testCSV() {
        String[] params = {"a111-08:30am-11:00am", "csv"};
        String expected = "type,room number,start time,end time\n" + "Auditorium,111,08:30am,11:00am";

        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testHTML(){
        String[] params = {"a111-08:30am-11:00am", "html"};
        String expected = "<dl>\n  <dt>Type</dt><dd>Auditorium</dd>\n  <dt>Room Number</dt><dd>111</dd>\n  <dt>Start Time</dt><dd>08:30am</dd>\n  <dt>End Time</dt><dd>11:00am</dd>\n</dl>";

        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testJSON() {
        String[] params = {"a111-08:30am-11:00am", "json"};

        String expected = "{\n" +
                String.format("  \"type\": \"%s\",\n", Booking.RoomType.Auditorium) +
                String.format("  \"roomNumber\": %s,\n", "111") +
                String.format("  \"startTime\": \"%s\",\n", "08:30am") +
                String.format("  \"endTime\": \"%s\"\n", "11:00am") +
                "}";

        app.main(params);

        assertEquals(expected, outContent.toString());
    }

}
